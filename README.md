# OpenML dataset: Weather-Istanbul-Data-2009-2019

https://www.openml.org/d/43724

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
Train your project by getting weather data from Istanbul.
Content
You can find daily weather, rainfall, sunrise time, sunset time, moonrise time, sunset time, average wind speed, average humidity and average pressure in Istanbul dataset.
Source
Daily observations were taken from the https://www.worldweatheronline.com website.
Inspiration
Estimating trained with tomorrow's weather binary classification method.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43724) of an [OpenML dataset](https://www.openml.org/d/43724). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43724/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43724/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43724/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

